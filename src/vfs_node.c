#include "../include/vfs_node.h"

#include "../include/vfs.h"
#include "../include/memory.h"

/**
 * Basic clas operations
 */
vfs_node* vfs_node_new()
{
    vfs_node* this = malloc(sizeof(vfs_node));
    if(this && vfs_node_constructor(this))
        return this;
    return 0;
}

void vfs_node_delete(vfs_node* this)
{
    if(this)
    {
        if(vfs_node_is_dir(this))
            vector_destructor(&((vfs_dirinfo*)(this->data))->nodes);
        free(this);
    }
}

uint8_t vfs_node_constructor(vfs_node* this)
{
    this->parent = 0;
    this->name   = 0;
    this->flags  = 0;
    this->data   = 0;
    return 1;
}

/**
 * Methods
 */
uint8_t vfs_node_is_file(const vfs_node* this)
{
    return !(this->flags & FS_DIRECTORY);
}

uint8_t vfs_node_is_dir(const vfs_node* this)
{
    return !!(this->flags & FS_DIRECTORY);
}

size_t vfs_file_get_size(const vfs_node* this)
{
    if(vfs_node_is_file(this))
    {
        return ((vfs_fileinfo*)(this->data))->size;
    }
    return 0;
}

// File stream internal routines

int vfs_file_seek(FILE* stream, long int offset, int origin)
{
    vfs_filestream* fstr = (vfs_filestream*)(stream->data);
    size_t fsize = vfs_file_get_size(fstr->node->data);
    switch(origin)
    {
        case SEEK_SET:
            if(offset >= 0 && (size_t)(offset) <= fsize)
            {
                fstr->pos = offset;
                return 0;
            }
            return EOF;
        case SEEK_CUR:
            if((size_t)(fstr->pos + offset) < fsize)
            {
                fstr->pos += offset;
                return 0;
            }
            return EOF;
        case SEEK_END:
            if(offset <= 0)
            {
                fstr->pos = fsize + offset;
                return 0;
            }
            return EOF;
    }
    return EOF;
}

int16_t vfs_file_getc(FILE* stream)
{
    vfs_filestream* fstr = (vfs_filestream*)(stream->data);
    if(fstr->pos + 1 <= vfs_file_get_size(fstr->node))
    {
        return (int16_t)(uint8_t)(((char*)(((vfs_fileinfo*)(fstr->node->data))->data))[fstr->pos++]);
    }
    return EOF;
}

size_t vfs_file_size(FILE* stream)
{
    vfs_filestream* fstr = (vfs_filestream*)(stream->data);
    return vfs_file_get_size(fstr->node);
}

void vfs_open_file(Filesystem* this, const char* name, const char* mode, FILE* stream)
{
    memset(stream, 0, sizeof(FILE));
    vfs_node* node = vfs_find_node(this, name);
    if(node && !(node->flags & FS_DIRECTORY))
    {
        stream->data = calloc(sizeof(vfs_filestream), 1);
        ((vfs_filestream*)(stream->data))->pos = 0;
        ((vfs_filestream*)(stream->data))->node = node;
        stream->seek = &vfs_file_seek;
        stream->size = &vfs_file_size;
        while(*mode)
        {
            switch(*mode)
            {
                case 'r':
                {
                    stream->getc = &vfs_file_getc;
                    break;
                }
            }
            ++mode;
        }
    }
}
