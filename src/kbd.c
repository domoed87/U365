#include <io.h>
#include <tty.h>
#include <memory.h>
#include <kbd.h>
#include <devices.h>

const char* scancode = "   1234567890-=\b^qwertyuiop[]\n^asdfghjkl;'`  zxcvbnm,./^*^   ^^^^^^^^^^^^^^789-456+1230.^^   !@#$%^&*()_+^^QWERTYUIOP{}\n^ASDFGHJKL:'^  ZXCVBNM<>?^*^   ^^^^^^^^^^^^^^&*(_$%^+!@#)>^^";

/* Keyboard functions. */
//Internal function used by getchar. Reads 1 scancode from keyboard buffer.
uint8_t kbd_getScancode()
{
    kbd_wait_irq();
    return inb(KBD_DATA);
}

const char* kbd_gets()
{
    char *cmd=malloc(128);
    uint8_t c=0;
    bool caps=false, shift=false;
    int i=0;
    while(true)
    {
        c=kbd_getScancode();
        if(c>0)
        {
           if(c==28)
           {
               cmd[i]='\0';
               return cmd;
           }
           if(c==0x3a)
           {
               caps=!caps;
               continue;
           }
           if(c==0x2a)
           {
               shift=true;
               continue;
           }
           if(c==0xaa)
           {
               shift=false;
               continue;
           }
           if(c==0x0e)
           {
                if(i>0)
                {
                    i--;
                    tty_goToPos(tty_x-1, tty_y);
                    tty_putchar(' ');
                    tty_goToPos(tty_x-1, tty_y);
                }
                continue;
           }
           if(c>0x01 && c<0x81)
           {
               if(!caps && !shift)
               {
                   tty_putchar(scancode[c+1]);
                   cmd[i++]=scancode[c+1];
               }
               else
               {
                   tty_putchar(scancode[c+1+90]);
                   cmd[i++]=scancode[c+1+90];
               }
            
           }
        }
    }
    return NULL;
}
/*
This function is waits for keyboard buffer to empty.
*/
void kbd_waitForBufToEmpty()
{
    char c=inb(0x60);
    while(inb(0x60)==c)
        c=inb(0x60);
}

//Keyboard-powered CPU reset.
void kbd_reset_cpu()
{
    asm("cli");
    uint8_t good = 0x02;
    while (good & 0x02)
        good = inb(0x64);
    outb(0x64, 0xFE);
    tty_wrstr("Keyboard CPU reset failed.");
    asm("hlt");
}