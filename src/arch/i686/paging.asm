global load_pg_dir
extern tty_wrstr
text db "Paging code debug. Halting",0
load_pg_dir:
	push ebp
	mov ebp, esp
	mov eax, [esp+8]
	mov cr3, eax
	mov esp, ebp
	pop ebp
	ret
mesg_wr:
	push text
	call tty_wrstr
	cli
	hlt
global paging_enable
paging_enable:
	cli
	push ebp
	mov ebp, esp
	mov eax, cr0
	or eax, 0x80000000
	mov cr0, eax
	call mesg_wr
	mov esp, ebp
	pop ebp
	ret
