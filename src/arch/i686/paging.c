#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <paging.h>
struct page_directory pgdir[1024] __attribute__((aligned(4096)));
struct page_table     pgtbl[1024] __attribute__((aligned(4096)));
void setup_pgdir(struct page_directory pgdir[1024])
{
	for(int i=0; i<1024; i++)
	{
		//Those page directory entries AREN'T USABLE. It just fills all page directory by not present page tables.
		pgdir[i].present=0;
		pgdir[i].rw_flag=1;
		pgdir[i].access_lvl=0;
		
	}
}
void paging_ident(uint32_t* tbl)
{
	size_t size=4096*1024;
	uint32_t from=0x0;
	//Code from OSDev.org.
	from &= 0xfffff000; // discard bits we don't want
    for(;size>0;from+=4096,size-=4096,tbl++){
       *tbl=from|1;     // mark page present.
    }
}
void setup_paging()
{
	void load_pg_dir(uint32_t*);
	void paging_enable(void);
	setup_pgdir(pgdir);
	paging_ident((uint32_t*)pgtbl);
	load_pg_dir((uint32_t*)pgdir);
	paging_enable();
}