#include <stdio.h>
#include <idt.h>
#include <gdt.h>
#include <io.h>
#include <sys.h>
#include <debug.h>
#include <devices.h>
#include <paging.h>
#include <power_mngmt.h>
#include <asmdefs.h>

/**
 * This file is part of U365 kernel.
 * DESC
 * Initializing of arch-specific features.
 */

void arch_init() //Initializing of x86-specific features.
{
    int detect_cpu(void);
    outb(0x21,0xfd);
    outb(0xa1,0xff);
    //debug_print(NOTICE,"Detecting SMBIOS...");

    unsigned char *mem = (unsigned char *) 0xF0000;
    int length, i;
    unsigned char checksum;
    while ((unsigned int) mem < 0x100000)
    {
        if (memcmp(mem,"_SM_",4)==0)
        {
            length = mem[5];
            checksum = 0;
            for(i = 0; i < length; i++)
            {
                checksum += mem[i];
            }
            if(checksum == 0) break;
        }
        mem += 16;
    }

    //serial_out("sm\n");
    if ((unsigned int) mem == 0x100000)
    {
        debug_print(WARNING,"No SMBIOS found.");
        //serial_out("smerr\n");
    }
    smb_ep = (struct SMBIOSEntryPoint*)mem;
    detectCPUSpeed();
    machine_bios_vendor=  detectBIOSVendor();
    machine_bios_version=detectBIOSVersion();
    machine_bios_bdate=    detectBIOSBDate();
    //serial_out("cpu speed detected\n");
    if(memcmp(mem,"_SM_",4)==0)
    {
        smbios_detected=true;
        printf("SMBIOS detected at 0x%08X\n", mem);
    }
    else
    {
        debug_print(WARNING,"SMBIOS entry point not found. OS functions which are displaying PC hardware info won't work.");
    }
    //debug_print(NOTICE,"Setting up PIC... ");
    init_pics(0x20,0x28);
    //debug_print(NOTICE,"Setting up GDT... ");
    gdt_install();
    //debug_print(NOTICE,"Setting up IDT... ");
    idt_install();
    //debug_print(NOTICE,"Setting up ISRs...");
    isrs_install();
    //debug_print(NOTICE,"Setting up IRQs...");
    irq_install();

    setupPIT();
    initKbdInt();
    //outb(0x21,0xfc);
    //outb(0xa1,0x00);
    //setup_paging();
    for(int i = 0; i < 16; ++i)
    {
        IRQ_clear_mask(i);
    }
    INT_ON;
    init_acpi();
    tty_wrstr("\nCPU Info:\n");
    detect_cpu();
}
