#include <io.h>
#include <time.h>
#include <sys.h>
int getCPUFreq()
{
	if(__u365_init_done)
		return cpuMHZ;
	uint64_t tsc=rdtsc();
	sleep(1000);
	uint64_t tsc2=rdtsc();
	return (tsc-tsc2)/1000/1000;
}