
#include "../include/mboot.h"
#include "../include/debug.h"
#include "../include/vesa_fb.h"
#include "../include/ish.h"
#include "../include/memory.h"
#include "../include/stdio.h"
#include "../include/sys.h"
#include "../include/tty.h"
#include "../include/init.h"
#include "../include/surface.h"
#include "../include/font.h"
#include "../include/string.h"
#include "../include/devices.h"
#include "../include/bmp.h"

/**
 * U365 Kernel entry. Called by _start, doing pre-run tasks and running the shell.
 */

void kernel_main(int eax_magic, struct mboot_info *mboot)
{
    init_srtInfo();
    initVBE(mboot);
    initTTY();
    tty_wrstr("U365 kernel is loaded. Checking bootloader magic number...");
    //If the EAX magic number passed by GRUB isn't valid, display error and stop boot process.
    if(eax_magic!=0x2BADB002)
    {
        tty_putchar('\n');
        fatal_error(EBOOT_MAGIC_ERROR);
    }
    tty_wrstr("OK.\n");
    tty_wrstr("TTY and VBE are initializd. Running os_lowlevel_init()...\n");
    // printf() uses memory allocating, which will be initialized later
    #ifdef NOT_FOR_RELEASE
    debug_print(WARNING, "This version is an unstable version from Git. Not for daily use, only for BPS developers.");
    #endif
    tty_putchar('\n');
    //Run pre-run tasks and start the shell.
    os_lowlevel_init(mboot);
    debug_print(SUCCESS,"OS is initialized. Loading the shell...");

    color_rgba cb = {0xFF, 0x99, 0x35, 0xFF};
    FILE* fi = fopen("/fonts/Font.bfn", "r");
    font* fo = font_open(fi);
    //fclose(fi);
    // Rewrite memory
    const char* text = "Okay, let's go. U365 rulezzz! BFN, VFS, STDIO, ish2 and Surface by k1-801. А бублики летают";
    uint32_t* tt = (uint32_t*)(calloc(strlen(text) + 1, 4));

    utf8_convert(text, tt);
    surface* sf = font_draw_unicode(fo, tt, color_black);
    printf("Rendered: %ix%i\n", sf->w, sf->h);
    rect r4 = {10, 550, sf->w, sf->h};
    rect r5 = {r4.x - 5, r4.y - 5, r4.w + 10, r4.h + 10};
    surface_fill_rect(surface_screen, cb, r5);
    surface_apply(surface_screen, sf, r4);
    surface_delete(sf);
    free(tt);

    FILE* imgf = fopen("/bmp.bmp", "r");
    surface* imgs = bmp_read(imgf);
    rect ri = {surface_screen->w - imgs->w - 20, surface_screen->h - imgs->h - 20, imgs->w, imgs->h};
    surface_apply(surface_screen, imgs, ri);
    printf("Image applied: %ix%i\n", (int)(imgs->w), (int)(imgs->h));
    surface_delete(imgs);
    fclose(imgf);
    
    ishMain();
    while(1);
}

