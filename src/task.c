#include "../include/task.h"
#include "../include/stdio.h"
#include "../include/memory.h"

static Task *runningTask;
static Task mainTask;
static Task otherTask;
static Task otherTask1;
static Task otherTask2;
static Task otherTask3;
static Task otherTask4;
 
static void otherMain() {
    printf("A"); // Not implemented here...
    preempt();
}
static void otherMain1() {
    printf("E"); // Not implemented here...
    preempt();
}
static void otherMain2() {
    printf("B"); // Not implemented here...
    preempt();
}
static void otherMain3() {
    printf("C"); // Not implemented here...
    preempt();
}
static void otherMain4() {
    printf("D"); // Not implemented here...
    preempt();
}


void initTasking() {
    // Get EFLAGS and CR3
    asm volatile("movl %%cr3, %%eax; movl %%eax, %0;":"=m"(mainTask.regs.cr3)::"%eax");
    asm volatile("pushfl; movl (%%esp), %%eax; movl %%eax, %0; popfl;":"=m"(mainTask.regs.eflags)::"%eax");
 
    createTask(&otherTask, otherMain, mainTask.regs.eflags, (uint32_t*)mainTask.regs.cr3);
    createTask(&otherTask1, otherMain1, otherTask.regs.eflags, (uint32_t*)mainTask.regs.cr3);
    createTask(&otherTask2, otherMain2, otherTask1.regs.eflags, (uint32_t*)mainTask.regs.cr3);
    createTask(&otherTask3, otherMain3, otherTask2.regs.eflags, (uint32_t*)mainTask.regs.cr3);
    createTask(&otherTask4, otherMain4, otherTask3.regs.eflags, (uint32_t*)mainTask.regs.cr3);
    mainTask.next = &otherTask;
    otherTask.next = &otherTask1;
    otherTask1.next = &otherTask2;
    otherTask2.next = &otherTask3;
    otherTask3.next = &otherTask4;
    otherTask4.next = &mainTask;

    runningTask = &mainTask;
}
 
void createTask(Task *task, void (*main)(), uint32_t flags, uint32_t *pagedir) {
    task->regs.eax = 0;
    task->regs.ebx = 0;
    task->regs.ecx = 0;
    task->regs.edx = 0;
    task->regs.esi = 0;
    task->regs.edi = 0;
    task->regs.eflags = flags;
    task->regs.eip = (uint32_t) main;
    task->regs.cr3 = (uint32_t) pagedir;
    task->regs.esp = (uint32_t) malloc(1024) + 0x1000; // Not implemented here
    task->next = 0;
}
 
void preempt() {
    Task *last = runningTask;
    runningTask = runningTask->next;
    switchTask(&last->regs, &runningTask->regs);
}
void doIt()
{
    printf("main...\n");
    preempt();
    printf("returned, lol");
}
