#include <debug.h>
#include <stdarg.h>
#include <stdio.h>
#include <asmdefs.h>

const char *ferrs    [64] = {
	"Your PC isn't compatible with U365. Sorry."												  ,
	"ACPI: RSDP signature invalid"																  ,
	"ACPI: RSDP checksum invalid"																  ,
	"ACPI: RSDT invalid" 																		  ,
	"ACPI: FADT not found"																		  ,
	"ACPI: Error while DSDT parsing: Invalid DSDT signature"									  ,
	"ACPI: Error while DSDT parsing: No FADT (maybe parse_dsdt() is called before FADT was found)",
	"ACPI: Cannot enable ACPI."																	  ,
	"ACPI: RSDT signature is valid, but checksum is wrong. Aborting."							  ,
	"ACPI: No \\_S5 object in DSDT."                                                              ,
	"FATAL BOOT ERROR: Invalid Multiboot magic number (EAX magic)."                               ,

};
int errorlevel=1;
const char *notices  [16] = {"[NOTICE ] ", "[SUCCESS] ", "[WARNING] ", "[ ERROR ] ", "[ FATAL ] ", "[ PANIC ] "};
uint8_t notcolors[16] = {0x09, 0x02, 0x0e, 0x04, 0x04, 0x0c};
void debug_print(int lvl, const char* str)
{
    if(lvl<errorlevel)
    	return;
   	tty_setcolor(notcolors[lvl]);
    tty_wrstr(notices[lvl]);
    tty_setcolor(0x07);
    tty_wrstr(str);
    tty_wrstr("\n");
}

void fatal_error(int msg)
{
	debug_print(PANIC,ferrs[msg]);
	INT_OFF;
	HALT   ;
}