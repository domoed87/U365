#include <ish.h>

#include "../include/cmds.h"
#include "../include/stdio.h"
#include "../include/panic.h"
#include "../include/string.h"
#include "../include/task.h"

// Top-level environment variables
env_descriptor* env;

command_table_t cmds[MAX_COMMANDS];
int cmdNum=0;

void noSuchCmd(const char *cmd)
{
    printf("ish: no such command: %s", cmd);
}

void addCmd(const char *name, const char *desc, void (*fcn)())
{
    if(cmdNum + 1 < MAX_COMMANDS)
    {
        strcpy(cmds[cmdNum].name,        name);
        strcpy(cmds[cmdNum].description, desc);
        cmds[cmdNum].action=fcn;
        ++cmdNum;
    }
}

uint8_t parseCmd(int argc, char **argv)
{
    if(!strcmp(argv[0], ""))
    {
        return 0;
    }

    for(int i = 0; i < cmdNum; i++)
    {
        if(strcmp(cmds[i].name, argv[0]) == 0)
        {
            void (*cmd_run)(int,char**) = cmds[i].action;
            cmd_run(argc, argv);
            return 1;
        }
    }
    return 0;
}

size_t argsize = 256;

int basreg(const char* text, char*** buf)
{
    uint8_t nbuf = 1;
    size_t i, j;
    int result = 0;
    char brackets = 0;
    *buf = 0;
    for(i = 0; text[i]; ++i)
    {
        if(nbuf)
        {
            ++result;
            *buf = (char**)(realloc(*buf, result * sizeof(char*)));
            (*buf)[result - 1] = (char*)(calloc(argsize, 1));
            j = 0;
            nbuf = 0;
        }
        if(text[i] == '\\' && text[i + 1])
        {
            switch(text[i + 1])
            {
                case '0':
                    (*buf)[result - 1][j++] = 0;
                    break;
                case 'n':
                    (*buf)[result - 1][j++] = '\n';
                    break;
                case 't':
                    (*buf)[result - 1][j++] = '\t';
                    break;
                default:
                    (*buf)[result - 1][j++] = text[i + 1];
            }
            ++i;
        }
        else
        {
            if(text[i] == '"' || text[i] == '\'')
            {
                if(!brackets)
                    brackets = text[i];
                else
                    if(brackets == text[i])
                        brackets = 0;
            }
            if(!brackets && (text[i] == ' ' || text[i] == '\t' || text[i] == '\n'))
            {
                nbuf = 1;
            }
            else
            {
                (*buf)[result - 1][j++] = text[i];
            }
        }
    }
    return result;
}

void ishMain()
{
    addCmd("",         "Empty command",                              emptyCmd);
    addCmd("help",     "Display known commands",                     help);
    addCmd("sysinfo",  "System info (only with SMBIOS)",             sysinfo);
    addCmd("ascii",    "Display ASCII table.",                       atable);
    addCmd("gugdun",   "Easter egg",                                 kotiki);
    addCmd("panic",    "Panic",                                      gpf);
    addCmd("smbios",   "SMBIOS Area Contents",                       smb_data);
    addCmd("testvfs",  "VFS test",                                   cmd_vfs_test);
    addCmd("showlogo", "show U365 logo in the middle of the screen", showlogo);
    addCmd("ish2",     "Run next generation Integrated Shell",       ish2);
    addCmd("cat",      "Show contents of specified files",           cmd_cat);
    addCmd("echo",     "Output all passed arguments",                cmd_echo);
    addCmd("reboot",   "Reboot your PC.",                            cmd_reboot);

    while(1)
    {
        printf("\nish1.0 : > ");
        kbd_waitForBufToEmpty();
        const char* input=kbd_gets();
        int argc;
        char** argv;
        argc = basreg(input, &argv);
        printf("\n");
        if(!parseCmd(argc, argv))
            noSuchCmd(input);
    }
}

void ish2()
{
    static uint8_t ready = 0;
    if(!ready)
    {
        env = calloc(sizeof(env_descriptor), 0);
        env->parent = 0;
        env->tree = string_tree_new();
        env_set(env, "USER",     "root");
        env_set(env, "HOSTNAME", "localhost");
        env_set(env, "UID",      "0");
        env_set(env, "GID",      "0");
        env_set(env, "PWD",      "/");
        env_set(env, "HOME",     "/root");
        env_set(env, "PS",      "root@localhost: # ");
        printf("USER    =%s\n"
               "HOSTNAME=%s\n"
               "UID     =%s\n"
               "GID     =%s\n"
               "PWD     =%s\n"
               "HOME    =%s\n"
               "PS      =%s\n\n", env_get(env, "USER"), env_get(env, "HOSTNAME"), env_get(env, "UID"), env_get(env, "GID"), env_get(env, "PWD"), env_get(env, "HOME"), env_get(env, "PS"));
    }
    while(1)
    {
        printf("%s", env_get(env, "PS"));
        const char* input = kbd_gets();
        printf("\n");
        int argc;
        char** argv;
        argc = basreg(input, &argv);
        printf("Debug: command: \"%s\"\n", argv[0]);
        if(!strcmp(argv[0], "exit"))
            return;
        if(!parseCmd(argc, argv))
        {
            printf("Debug: this command should be checked");
        }
        printf("\n");
        for(size_t i = 0; i < (size_t)argc; ++i)
        {
            free(argv[i]);
        }
        free(argv);
    }
}

const char* env_get(env_descriptor* this, const char* key)
{
    //printf("Tree: 0x%08x, 'P': 0x%08x\n", this->tree, this->tree->table['P']);
    const char* result = string_tree_get(this->tree, key);
    if(!result && this->parent)
        return env_get(this->parent, key);
    return result;
}

void env_set(env_descriptor* this, const char* key, const char* value)
{
    string_tree_set(this->tree, key, value);
}