#include "../include/string_tree.h"

#include "../include/string.h"
#include "../include/memory.h"

string_tree* string_tree_new()
{
    string_tree* this = malloc(sizeof(string_tree));
    if(this && string_tree_constructor(this))
        return this;
    return 0;
}

void string_tree_delete(string_tree* this)
{
    string_tree_destructor(this);
    free(this);
}

uint8_t string_tree_constructor(string_tree* this)
{
    memset(this, 0, sizeof(string_tree));
    return 1;
}

void string_tree_destructor(string_tree* this)
{
    uint16_t i;
    for(i = 0; i < 256; ++i)
    {
        string_tree_delete(this->table[i]);
    }
    free(this->value);
}

const char* string_tree_get(const string_tree* this, const char* key)
{
    const string_tree* t = this;
    while(*key)
    {
        t = t->table[(int)(*key)];
        if(!t)
            return 0;
        ++key;
    }
    return t->value;
}

void string_tree_set(string_tree* this, const char* key, const char* value)
{
    string_tree* t = this;
    while(*key)
    {
        if(!t->table[(int)(*key)])
            t->table[(int)(*key)] = string_tree_new();
        t = t->table[(int)(*key)];
        ++key;
    }
    if(t->value)
        free(t->value);
    t->value = calloc(strlen(value) + 1, 1);
    strcpy(t->value, value);
}