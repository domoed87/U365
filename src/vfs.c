#include "../include/vfs.h"

#include "../include/memory.h"
#include "../include/string.h"

#include "../include/file_Font_bfn.h"
#include "../include/file_bmp_bmp.h"
#include "../include/file_logo_bmp.h"

uint8_t vfs_create_file(Filesystem*, const char*, fs_flags_t);
uint8_t vfs_delete_file(Filesystem*, const char*);
uint8_t vfs_create_dir (Filesystem*, const char*, fs_flags_t);
uint8_t vfs_delete_dir (Filesystem*, const char*);
void    vfs_open_file  (Filesystem*, const char*, const char*, FILE*);

Filesystem* root = 0;
char *vfs_test_file="U365 VFS test.\n\
If you see this text, U365 VFS is working normal.\n\
Made by BPS. Developers:\n\
catnikita255 & k1-801";
char *vfs_license_file="U365 by BPS Dev Team is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.\n"
                             "Anybody can use it in their projects with some exceptions.\n"
                             "You MUST leave that license text in VFS root (\"/\"), named \"license.txt\".\n"
                             "You MUST leave original authors, the BPS Dev Team (this is us), as the original authors. Example:\n"
                             "> cat /authors.txt\n"
                             "Original Authors    - BPS Dev Team - catnikita255 & k1-801\n"
                             "Forked and improved - The FooBar team/company/etc - bsmith145, branf and Neo1975";
char *vfs_os_api_file= "U365 API documentation.\n\n"
                       "Basic STDIO functions\n\n"
                       "printf(const char* fmt, ...)\n"
                       "==============================\n"
                       "Prints formatted string to stdout. All format specifiers are supported.\n"
                       "";
void vfs_set_contents(vfs_node* node, uint8_t *data, int len)
{
    if(node)
    {
        ((vfs_fileinfo*)(node->data))->data = data;
        ((vfs_fileinfo*)(node->data))->size = len;
    }
    else
        perr("vfs_set_contents: invalid node - maybe no such file?");
}

void init_rootfs()
{
    root = vfs_new();

    fs_create_dir (root, "bin"     , 0x1FC);
    fs_create_dir (root, "dev"     , 0x1FC);
    fs_create_dir (root, "proc"    , 0x1FC);
    fs_create_dir (root, "run"     , 0x1FC);
    fs_create_dir (root, "sys"     , 0x1FC);
    fs_create_dir (root, "usr"     , 0x1FC);
    fs_create_dir (root, "fonts"   , 0x1FC);

    fs_create_file(root, "fonts/Font.bfn", 0x124);
    fs_create_file(root, "test.txt", 0x124);
    fs_create_file(root, "bmp.bmp", 0x124);
    fs_create_file(root, "fake", 0x124);
    fs_create_file(root, "logo.bmp", 0x124);
    fs_create_file(root, "license.txt", 0x124);
    fs_create_file(root, "fake2", 0x124);
    fs_create_file(root, "os_api_doc.txt", 0x124);


    vfs_node*  font_node = vfs_find_node(root, "/fonts/Font.bfn");
    vfs_node*  test_node = vfs_find_node(root, "test.txt");
    vfs_node* image_node = vfs_find_node(root, "bmp.bmp");
    vfs_node*  logo_node = vfs_find_node(root, "logo.bmp");
    vfs_node*  license_node = vfs_find_node(root, "license.txt");
    vfs_node*  osapidoc_node = vfs_find_node(root, "os_api_doc.txt");

    vfs_set_contents(font_node,file_Font_bfn,file_Font_bfn_size);
    vfs_set_contents(test_node,(uint8_t *)vfs_test_file,strlen(vfs_test_file));
    vfs_set_contents(image_node,file_bmp_bmp,file_bmp_bmp_size);
    vfs_set_contents(logo_node,file_logo_bmp,file_logo_bmp_size);
    vfs_set_contents(license_node,(uint8_t *)vfs_license_file,strlen(vfs_license_file));
    vfs_set_contents(osapidoc_node,(uint8_t *)vfs_os_api_file,strlen(vfs_os_api_file));
}

// VFS
/**
 * Basic class operations
 */
Filesystem* vfs_new()
{
    Filesystem* this = malloc(sizeof(Filesystem));
    if(this && vfs_constructor(this))
        return this;
    return 0;
}

void vfs_delete(Filesystem* this)
{
    if(this)
    {
        vfs_node_delete((vfs_node*)(this->data));
        if(this->parent)
        {
            vector* v = &this->parent->mounted;
            vector_erase_by_value(v, this);
        }
        free(this);
    }
}

uint8_t vfs_constructor(Filesystem* this)
{
    this->data = calloc(sizeof(vfs_node), 1);
    ((vfs_node*)(this->data))->name = "";
    ((vfs_node*)(this->data))->data = calloc(sizeof(vfs_dirinfo), 1);

    this->create_file = &vfs_create_file;
    this->delete_file = &vfs_delete_file;
    this->create_dir  = &vfs_create_dir;
    this->delete_dir  = &vfs_delete_dir;
    this->open_file   = &vfs_open_file;
    return 1;
}

/**
 * Methods
 */
vfs_node* vfs_find_node(Filesystem* this, const char* name)
{
    vfs_node* node = (vfs_node*)(this->data);
    while(name && *name)
    {
        while(*name == '/') ++name;
        size_t i;
        for(i = 0; name[i] && name[i] != '/'; ++i);
        char* buf = (char*)(calloc(i + 1, 1));
        memcpy(buf, name, i);
        name += i;
        vector* v = &((vfs_dirinfo*)(node->data))->nodes;
        node = 0;
        for(i = 0; i < vector_size(v); ++i)
        {
            vfs_node* next = *(vfs_node**)(vector_at(v, i));
            if(!strcmp(next->name, buf))
            {
                node = next;
                break;
            }
        }
        if(!node || (*name && !(node->flags & FS_DIRECTORY)))
        {
            return 0;
        }
    }
    return node;
}

vfs_node* vfs_create_node(Filesystem* this, const char* name, fs_flags_t flags)
{
    size_t i = 0;
    size_t name_len = strlen(name);
    size_t lastsl = 0;
    for(i = 0; i < name_len - 1; ++i)
    {
        if(name[i] == '/')
        {
            lastsl = i + 1;
        }
    }

    char* prefix = calloc(           lastsl + 1, 1);
    char* nname  = calloc(name_len - lastsl + 1, 1);
    memcpy(prefix, name,          lastsl - !!lastsl);
    memcpy(nname,  name + lastsl, name_len - lastsl);

    vfs_node* prenode = vfs_find_node(this, prefix);
    free(prefix);
    if(!prenode)
    {
        free(nname);
        return 0;
    }

    vfs_node* newnode = vfs_node_new();
    if(newnode)
    {
        newnode->name   = nname;
        newnode->flags  = flags;
        newnode->parent = prenode;
        if(flags & FS_DIRECTORY)
        {
            newnode->data = calloc(sizeof(vfs_dirinfo), 1);
            ((vfs_dirinfo*)(newnode->data))->nodes.del = (void(*)(void*))(&vfs_node_delete);
        }
        else
        {
            newnode->data = calloc(sizeof(vfs_fileinfo), 1);
        }
        vector_push_back(&(((vfs_dirinfo*)(prenode->data))->nodes), newnode);
    }
    return newnode;
}

uint8_t vfs_delete_node(Filesystem* this, const char* name)
{
    vfs_node* node = vfs_find_node(this, name);
    if(node)
    {
        vfs_node* prenode = node->parent;
        if(prenode)
        {
            vector* v = &(((vfs_dirinfo*)(prenode->data))->nodes);
            size_t i;
            size_t n = vector_size(v);
            for(i = 0; i < n; ++i)
            {
                if(!strcpy(node->name, (*(vfs_node**)(vector_at(v, i)))->name))
                {
                    break;
                }
            }
            for(; i < n - 1; ++i)
            {
                *vector_at(v, i) = *vector_at(v, i + 1);
            }
            vector_pop_back(v);
        }
    }
    return 0;
}

uint8_t vfs_create_file(Filesystem* this, const char* name, fs_flags_t flags)
{
    return !!vfs_create_node(this, name, flags & (~FS_DIRECTORY));
}

uint8_t vfs_delete_file(Filesystem* this, const char* name)
{
    return vfs_delete_node(this, name);
}

uint8_t vfs_create_dir(Filesystem* this, const char* name, fs_flags_t flags)
{
    return !!vfs_create_node(this, name, flags | FS_DIRECTORY);
}

uint8_t vfs_delete_dir (Filesystem* this, const char* name)
{
    return vfs_delete_node(this, name);
}
