#include "../include/cmds.h"

#include "../include/ish.h"
#include "../include/stdio.h"
#include "../include/io.h"
#include "../include/kbd.h"
#include "../include/tty.h"
#include "../include/string.h"
#include "../include/panic.h"
#include "../include/surface.h"
#include "../include/bmp.h"

void emptyCmd(){}

void help(int argc, char **argv)
{
    int argc_orig=argc;
    if(argc>1)
    {
        while(argc-->=1)
        {
            int i;
            for(i=0; i<=256 && strcmp(cmds[i].name, argv[argc_orig-argc])!=0; i++)
                ;
            printf("%s\t\t%s\n", cmds[i].name, cmds[i].description);
        }
        return;
    }
    printf(
        "ISH 1.0\n"
        "U365 Kernel Integrated Shell. Known commands:\n");

    int maxlen = 0;
    int currlen;
    int i;

    for(i = 0; i < cmdNum; ++i)
    {
        currlen = strlen(cmds[i].name);
        if(maxlen < currlen)
        {
            maxlen = currlen;
        }
    }
    for(i = 0; i < cmdNum; ++i)
    {
        printf("[%-*s]    %s\n", maxlen, cmds[i].name, cmds[i].description);
    }
}

void sysinfo(int argc,char** argv)
{
    int argc_orig=argc;
    if(argc==1)
        printf(
            "U365 system info\n"
            "CPU Frequency       %d Mhz\n"
            "BIOS Vendor         %s\n"
            "BIOS Version        %s\n"
            "BIOS Release Date   %s\n",
            getCPUFreq(),
            (smbios_detected) ? detectBIOSVendor() : "ERR: No SMBIOS",
            (smbios_detected) ? detectBIOSVersion() : "ERR: No SMBIOS",
            (smbios_detected) ? detectBIOSBDate() : "ERR: No SMBIOS");
    else
    while(argc--)
    {
        if(!strcmp(argv[argc_orig-argc],"--help"))
            printf("U365 Kernel\n"
                   "--help\t\tDisplay this message\n"
                   "freq\t\tDisplay CPU Frequency\n"
                   "bvndr\t\tDisplay BIOS vendor\n"
                   "bver\t\tDisplay BIOS version\n"
                   "brldt\t\tDisplay BIOS release date\n"
                   "<no args>\tDisplay all system data.");
        if(!strcmp(argv[argc_orig-argc], "freq"))
                printf("CPU Frequency       %d Mhz\n",getCPUFreq());
        if(!strcmp(argv[argc_orig-argc], "bvndr"))
                printf("BIOS Vendor         %s\n",(smbios_detected) ? detectBIOSVendor() : "ERR: No SMBIOS");
        if(!strcmp(argv[argc_orig-argc], "bver"))
                printf("BIOS Version        %s\n",(smbios_detected) ? detectBIOSVersion() : "ERR: No SMBIOS");
        if(!strcmp(argv[argc_orig-argc], "brldt"))
                printf("BIOS Release Date   %s\n",(smbios_detected) ? detectBIOSBDate() : "ERR: No SMBIOS");
    }

}

void atable()
{
    for(int i=0; i<128; i++)
        tty_putchar(i);
}
void smb_data()
{
    char *sm=(char*)smb_ep;
    for(int i=0; i<1024; i++)
        tty_putchar(*sm++);
}

void kotiki()
{
    initTTY();
    printf("KOTIKI KOTIKI KOOOTEEEKEEEYYY\n");
    printf("/\\_/\\ ♥\n\
<^,^>\n\
/ \\\n\
(__)__\n\
");
    printf("___________________________$$$$$________$$$$______\n\
___________________________$_$$$$$$$$$$$$$$$______\n\
___________________________$__$$$______$$$_$______\n\
____________________________$_____________$$______\n\
___________________________$$____________$$_______\n\
___________________________$$__$$$$____$__$$______\n\
_______________________$$$$$$$$_$$$___$$$_$_______\n\
_______________________$$$$$$$$$$_________$_______\n\
____________________________$$$_$$_$$$_$$$$$$$$___\n\
__________________________$$$$$$$__$$$$$$$$$$__$__\n\
________________________$$$$$$_$$$$$$$$$$$___$____\n\
_______________________$$__$$_____$$$___$$_$______\n\
$$$$$$________________$$$___$__________$$$__$_____\n\
$$$__$$______________$$_____$$__________$$________\n\
__$$__$$____________$$______$$_________$$$________\n\
___$$__$$___________$$_______$$______$$$$$________\n\
____$__$$__________$$________$$$$___$$__$$________\n\
____$$__$$_______$$$$_______________$___$$________\n\
_____$$__$______$$______________________$$________\n\
_____$$__$$____$$_______________________$$________\n\
______$__$$___$$_______$$$$___$$___$$___$_________\n\
______$$__$$__$$________$$$___$$__$$___$$_________\n\
_______$__$$__$$__________$$___$$_$___$$__________\n\
_______$$__$$_$$__________$$___$$$$___$$__________\n\
________$$__$$$$___________$____$$$___$___________\n\
_________$$_$$$$$__________$$___$$$___$___________\n\
__________$$__$$$_________$$$$__$$$$__$$__________\n\
____________$$$$$_________$$$$___$$$___$$$________\n\
______________$$$$$____$_$_$$$$_$_$$__$_$$________\n\
__________________$$$$$$$$$$$$$$$$$$$$$$$_________\n\
_______________$$$$$$$$$__________________________\n");
    panic("CATS HAVE TAKEN OVER THE CONTROL!!!");
}

void gpf()
{
    asm("int $99");
}

void cmd_cat(int argc, char** argv)
{
    if(argc==1)
    {
        perr("cat: filename(s) expected");
        return;
    }
    if(argc>=64)
    {
        perr("cat: too many input files");
        return;
    }
    FILE* fp;
    int argc_orig=argc;
    while(argc-->1)
    {
        fp = fopen(argv[argc_orig-argc], "r");
        if(fp)
        {
            char c;
            while((c=fgetc(fp))!=EOF)
                tty_putchar(c);
            tty_putchar('\n');
            fclose(fp);
        }
        else
        {
            printf("Failed to open file: %s\n", argv[argc_orig-argc]);
        }
    }
}

void cmd_echo(int argc, char** argv)
{
    int argc_orig=argc;
    while(argc-- > 1)
    {
        printf("%s",argv[argc_orig-argc]);
        tty_putchar(' ');
    }
    tty_putchar('\n');
}

void cmd_vfs_test()
{
    printf("Calling fopen...\n");
    FILE* test_vfs = fopen("/test.txt", "r");
    printf("done. Starting read process...\n\n");
char c;
while((c=fgetc(test_vfs))!=EOF)
    tty_putchar(c);
    printf("\nSee result in terminal\n");
}

void showlogo()
{
    FILE* lf = fopen("/logo.bmp", "r");
    if(lf)
    {
        surface* ls = bmp_read(lf);
        if(ls)
        {
            rect lr =
            {
                (surface_screen->w - ls->w) / 2,
                (surface_screen->h - ls->h) / 2,
                (surface_screen->w + ls->w) / 2,
                (surface_screen->h + ls->h) / 2,
            };
            surface_apply(surface_screen, ls, lr);
            surface_delete(ls);
            return;
        }
    }
    printf("Failed to read logo\n");
    //fclose(lf);
}
void cmd_reboot()
{
    printf("System reboot\n");
    //Only kbd reboot is supported at this time.
    kbd_reset_cpu();
}