#include <idt.h>
#include <stdio.h>
#include <asmdefs.h>
void panic(const char *mesg)
{
	INT_OFF;
	printf("\npanic(): %s\n\n",mesg);
	if(panicr==0)
	{
		printf("Panic isn't called from interrupt handler, so the registers can not be displayed. Halting now.\n");
		HALT;
	}
	printf("Register state before a panic:\n");
	printf("EAX=0x%08x  EBX=0x%08x  ECX=0x%08x  EDX=0x%08x  EDI=0x%08x  ESI=0x%08x  EBP=0x%08x EIP=0x%08x  EFLAGS=%b\n",panicr->eax,panicr->ebx,panicr->ecx,panicr->edx,panicr->edi,panicr->esi,panicr->ebp,panicr->eip,panicr->eflags);
	printf("Error code (if present, otherwise 0): 0x%08X\n",panicr->err_code);
	printf("Interrupt number                    : %d %d\n",panicr->int_no);
	printf("Halting.");
	HALT;
}