#ifndef ISH_H
#define ISH_H

#define MAX_COMMANDS 100

#include <stdint.h>
#include <stddef.h>

#include "../include/memory.h"
#include "../include/string_tree.h"

typedef struct env_descriptor env_descriptor;

typedef struct
{
    char name[128];
    char description[128];
    void (*action);
} command_table_t;

extern command_table_t cmds[MAX_COMMANDS];
extern int cmdNum;
void ishMain();

// ish 2
struct env_descriptor
{
    env_descriptor* parent;
    string_tree* tree;
};

extern size_t argsize;
void ish2();

const char* env_get(env_descriptor*, const char*);
void        env_set(env_descriptor*, const char*, const char*);

#endif // ISH_H