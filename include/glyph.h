#ifndef GLYPH_H
#define GLYPH_H

#include <stdint.h>
#include "../include/stdio.h"
#include "../include/surface.h"

typedef struct glyph glyph;

struct glyph
{
    uint8_t** data;
    uint32_t w;
    uint32_t h;
    uint32_t c;
     int32_t o;
};

/**
 * Basic class operations
 */
glyph*  glyph_new();
void    glyph_delete     (glyph*);
uint8_t glyph_constructor(glyph*);
void    glyph_destructor (glyph*);

/**
 * Methods
 */
void    glyph_read(glyph*, FILE*, uint32_t, uint32_t);
void    glyph_draw(glyph*, surface*, color_rgba, rect);

#endif // GLYPH_H
