#ifndef __KBD_H
#define __KBD_H
/*
Something about this header.
It's a internal low-level header, used by the keyboard system. Do not include it in your files; use scrio.h instead.
*/
//Scancode table to get codes corresponding to each key. All ^s are non-printable characters as arrows, F* etc.
extern const char* scancode;

#define KBD_DATA 0x60

uint8_t     kbd_getScancode(); //Reading from KBD_DATA.
char        kbd_getchar();
const char* kbd_gets();
void        kbd_waitForBufToEmpty();
void 		kbd_reset_cpu();

#endif