#ifndef __DRAWING_H
#define __DRAWING_H
#include <stdint.h>
void hLine(int x, int y, int length, uint32_t color);
void vLine(int x, int y, int length, uint32_t color);
void fillRect(int,int,int,int,uint32_t);
void putLine( int x0, int y0, int x1, int y1, uint32_t color );
void drawBitmap(int,int,uint32_t[],int,int);
#endif