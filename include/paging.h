#ifndef __PAGING_H
#define __PAGING_H
struct page_directory {
	int present       : 1;
	int rw_flag       : 1;
	int access_lvl    : 1; //0 is for only ring0. 1 is for anybody.
	int write_through : 1;
	int cache_off     : 1;
	int accessed      : 1;
	int zero          : 1;
	int page_size     : 1;
	int reserved      : 3;
	int tbl_addr      : 21;
} __attribute__((packed));
struct page_table {
	int present       : 1;
	int rw_flag       : 1;
	int access_lvl    : 1; //0 is for only ring0. 1 is for anybody.
	int cache_off     : 1;
	int accessed      : 1;
	int dirty         : 1;
	int zero          : 1;
	int global        : 1;
	int reserved      : 3;
	int tbl_addr      : 21;
} __attribute__((packed));
void setup_pgdir(struct page_directory pgdir[1024]);
void paging_setup();
#endif