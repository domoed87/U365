#ifndef __PANIC_H
#define __PANIC_H
#include <idt.h>
void panic(const char *);
#endif