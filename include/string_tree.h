#ifndef STRING_TREE_H
#define STRING_TREE_H

#include <stddef.h>
#include <stdint.h>

typedef struct string_tree string_tree;

struct string_tree
{
    string_tree* table[256];
    char* value;
};

string_tree* string_tree_new();
void         string_tree_delete     (string_tree*);
uint8_t      string_tree_constructor(string_tree*);
void         string_tree_destructor (string_tree*);

const char* string_tree_get(const string_tree*, const char*);
void        string_tree_set(      string_tree*, const char*, const char*);

#endif // STRING_TREE_H