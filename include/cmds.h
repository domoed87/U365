#ifndef CMDS_H
#define CMDS_H

void help(int,char**);
void sysinfo(int,char**);
void atable();
void emptyCmd();
void kotiki();
void gpf();
void smb_data();
void cmd_cat(int,char**);
void cmd_echo(int, char**);
void cmd_vfs_test();
void cmd_reboot();
void showlogo();
void ish2();

#endif // CMDS_H