#ifndef __STDIO_H
#define __STDIO_H

#include <stdint.h>
#include "../include/tty.h"
#include "../include/kbd.h"
#include "../include/fio.h"

extern FILE* stdin;
extern FILE* stdout;
extern FILE* stderr;

#ifdef UNUSED
#elif defined(__GNUC__)
# define UNUSED(x) UNUSED_ ## x __attribute__((unused))
#elif defined(__LCLINT__)
# define UNUSED(x) /*@unused@*/ x
#else
# define UNUSED(x) x
#endif

int   printf(       const char *fmt, ...);
int  vprintf(       const char *fmt, va_list);
int  sprintf(char*, const char *fmt, ...);
int vsprintf(char*, const char *fmt, va_list);

#endif
