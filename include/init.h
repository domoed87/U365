#ifndef __INIT_H
#define __INIT_H

#include <debug.h>
#include <mboot.h>

void arch_init();
void os_lowlevel_init();

#endif