#ifndef __MEM_H
#define __MEM_H

#include <stdint.h>
#include <stddef.h>

typedef struct page_descriptor page_descriptor;
typedef struct page_table      page_table;

struct page_descriptor
{
//    uint64_t p_id;
    uint16_t p_free;
    uint16_t p_busy;
    void*    p_data;
};

struct page_table
{
    size_t           p_count;
    page_descriptor* p_table;
    void*            p_data;
};

void* memcpy (void*, const void*, size_t);
void* memset (void*, uint8_t, size_t);
void* memmove(void*, void*, size_t);
// Old heap
void* malloc (size_t);
void* calloc (size_t, size_t);
void* realloc(void*, size_t);
void  free   (void*);
// New heap
void* malloc2 (size_t);
void* calloc2 (size_t, size_t);
void* realloc2(void*, size_t);
void  free2   (void*);

#endif
