#ifndef __IO_H
#define __IO_H
#include <stdint.h>
#include <stdbool.h>
#define PIC1 0x20
#define PIC2 0xA0
#define ICW1 0x11
#define ICW4 0x01
#define PIC1		0x20		/* IO base address for master PIC */
#define PIC2		0xA0		/* IO base address for slave PIC */
#define PIC1_COMMAND	PIC1
#define PIC1_DATA	(PIC1+1)
#define PIC2_COMMAND	PIC2
#define PIC2_DATA	(PIC2+1)
uint8_t  inb(uint16_t port);                //Reads 1 byte from port.
uint16_t inw(uint16_t port);                //Reads 2 bytes from port.
uint32_t inl(uint16_t port);                //Reads 4 bytes from port.
void     outb(uint16_t port, uint8_t  val); //Outputs 1 byte to port.
void     outw(uint16_t port, uint16_t val); //Outputs 2 bytes to port.
void     outl(uint16_t port, uint32_t val); //Outputs 4 bytes to port.
uint64_t rdtsc();                           //Returns CPU uptime after last reset in ticks.
void     read_rtc();
extern unsigned char second, minute, hour, day, month;
extern unsigned int year;
void init_pics(int pic1, int pic2);
 struct SMBIOSEntryPoint {
 	char EntryPointString[4];    //This is _SM_
 	uint8_t Checksum;              //This value summed with all the values of the table, should be 0 (overflow)
 	uint8_t Length;                //Length of the Entry Point Table. Since version 2.1 of SMBIOS, this is 0x1F
 	uint8_t MajorVersion;          //Major Version of SMBIOS
 	uint8_t MinorVersion;          //Minor Version of SMBIOS
 	uint16_t MaxStructureSize;     //Maximum size of a SMBIOS Structure (we will se later)
 	uint8_t EntryPointRevision;    //...
 	char FormattedArea[5];       //...
 	char EntryPointString2[5];   //This is _DMI_
 	uint8_t Checksum2;             //Checksum for values from EntryPointString2 to the end of table
 	uint16_t TableLength;          //Length of the Table containing all the structures
 	uint32_t TableAddress;	     //Address of the Table
 	uint16_t NumberOfStructures;   //Number of structures in the table
 	uint8_t BCDRevision;           //Unused
 };
  typedef struct SMBIOSHeader {
 	uint8_t Type;
 	uint8_t Length;
 	uint16_t Handle;
 }SMBIOSHeader;
 struct SMBIOSEntryPoint *smb_ep;
uint16_t cpuMHZ;
void detectCPUSpeed();
const char *detectBIOSVendor ();
const char *detectBIOSVersion();
const char *detectBIOSBDate  ();
extern bool smbios_detected;
void insl(unsigned short port, unsigned int buffer, unsigned long count);
int  getCPUFreq();
#endif