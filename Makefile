ARROW=\e[1;34m==>\e[1;37m

#U365 Makefile configuration

LSCRIPT    = link
BUILD_DIR  = build
SRC_DIR    = src
INC_DIR    = include
VFS_DIR    = vfs
OS_VER     = "0.6"

#Filenames

ARCH       = i686
BINFORMAT  = elf
BITS       = 32
ISODIR     = $(BUILD_DIR)/iso/fs
GRUB_BIN   = /usr/lib/grub/i386-pc/
CROSS      = ~/opt/cross
CROSSBIN   = $(CROSS)/bin/
GAS        = $(CROSSBIN)$(ARCH)-$(BINFORMAT)-as #AT&T-syntax assembler
CC         = $(CROSSBIN)$(ARCH)-$(BINFORMAT)-gcc
IASM       = yasm #Intel-syntax assembler
BINFILE    = u365#Compiled ELF binary and ISO image name.
EMU        = qemu-system-i386
EFLAGS     = -cdrom $(BINFILE).iso
DEFS       = -DOSVER=$(OS_VER)
MKRSCFLAGS = -d $(GRUB_BIN) -o $(BINFILE).iso $(ISODIR)
CFLAGS     = -O3 -ffreestanding -Wall -Wextra -Wmaybe-uninitialized -fno-exceptions -std=gnu11 -Iinclude -c $(DEFS)
GASFLAGS   =
IASFLAGS   = -f $(BINFORMAT)$(BITS)
LDFLAGS    = -T $(LSCRIPT).ld -o $(BUILD_DIR)/bin/$(BINFILE).$(BINFORMAT) -O2 -nostdlib

# Source file names computing
# Multiboot header first
SOURCES  := $(SRC_DIR)/arch/$(ARCH)/boot.s
# Al common sources
SOURCES  += $(shell find $(SRC_DIR) -name "*.c"   -and -not -path "$(SRC_DIR)/arch/*" -type f -print)
SOURCES  += $(shell find $(SRC_DIR) -name "*.s"   -and -not -path "$(SRC_DIR)/arch/*" -type f -print)
SOURCES  += $(shell find $(SRC_DIR) -name "*.asm" -and -not -path "$(SRC_DIR)/arch/*" -type f -print)
# Architecture-dependent sources
SOURCES  += $(shell find $(SRC_DIR)/arch/$(ARCH) -name '*.c' -type f -print)
SOURCES  += $(shell find $(SRC_DIR)/arch/$(ARCH) -name '*.s' -and -not -path "$(SRC_DIR)/arch/$(ARCH)/boot.s" -type f -print)
SOURCES  += $(shell find $(SRC_DIR)/arch/$(ARCH) -name '*.asm' -type f -print)

SRCDIRS  := $(shell find $(SRC_DIR) -type d -print)
VFSDIRS  := $(shell find $(VFS_DIR) -type d -print)
OBJDIRS  := $(patsubst $(SRC_DIR)/%,"$(BUILD_DIR)/obj/%",$(SRCDIRS))
CFSDIRS  := $(patsubst $(VFS_DIR)/tree/%,"$(VFS_DIR)/conv/%",$(VFSDIRS))

FAKE := $(shell echo $(SOURCES))
# Object file names computing
OBJS     := $(patsubst $(SRC_DIR)/%.c,"$(BUILD_DIR)/obj/%.c.o",$(SOURCES))
OBJS     := $(patsubst $(SRC_DIR)/%.s,"$(BUILD_DIR)/obj/%.s.o",$(OBJS))
OBJS     := $(patsubst $(SRC_DIR)/%.asm,"$(BUILD_DIR)/obj/%.asm.o",$(OBJS))

VFS_FILES := $(shell find $(VFS_DIR)/tree -type f -print)
VFS_HDRS  := $(patsubst $(VFS_DIR)/tree/%,"$(VFS_DIR)/conv/%",$(VFSDIRS))
VFS_VARNAME=$(patsubst "Variable name: %", "%", `bintoc -f $@ -v | grep --color=never "Variable name: "`)
VFS_SRCNAME=$(patsubst "$(VFS_DIR)/tree/%", "$(VFS_DIR)/conv/%", $(@D))
#End

# target      _welcome dependencies
all:          _welcome clean directories compile link iso run
	@echo -n

# Welcome user at make call
_welcome:
	@echo -e " \e[1;33mMakefile:\e[0m \e[1;32mU365\e[0m"

compile:     _welcome clean directories _compile $(SOURCES)
#	@echo -e " $(ARROW) Compiling GDT\e[0m"
#	@$(IASM) $(SRC_DIR)/arch/$(ARCH)/gdt.s -o $(BUILD_DIR)/obj/gdt.o $(IASFLAGS)
#	@echo -e " $(ARROW) Compiling IDT\e[0m"
#	@$(IASM) $(SRC_DIR)/arch/$(ARCH)/idt.s -o $(BUILD_DIR)/obj/idt.o $(IASFLAGS)
#	@echo -e " $(ARROW) Compiling C sources\e[0m"
#	@$(CC) $(SOURCES) $(SRC_DIR)/arch/$(ARCH)/init.c $(IOBJS) $(CFLAGS) $(LDFLAGS)
	@echo -n

link:        _welcome clean directories compile
	@echo -e " $(ARROW) Linking\e[0m"
	@$(CC) $(OBJS) $(LDFLAGS)


iso:         _welcome directories
	@echo -e " $(ARROW) Generating an ISO image\e[0m"

	@echo "insmod gfxterm;  \
insmod vbe;  \
timeout=5;   \
loadfont /boot/grub/fonts/unicode.pf2;  \
set gfxmode=1024x768;  \
terminal_output gfxterm;  \
menuentry "U365 Basic System 1.0"  \
{  \
	multiboot /boot/u365.elf;  \
	boot;  \
}  \
" > $(BUILD_DIR)/iso/fs/grub.cfg

	@cp $(BUILD_DIR)/iso/fs/grub.cfg $(BUILD_DIR)/iso/fs/boot/grub/grub.cfg
	@cp $(BUILD_DIR)/bin/$(BINFILE).$(BINFORMAT) $(BUILD_DIR)/iso/fs/boot/
	@grub-mkrescue $(MKRSCFLAGS) &> /dev/null
	@echo -n

run:         _welcome
	@echo -e " $(ARROW) Booting the ISO image\e[0m"
	$(EMU) $(EFLAGS)
	@echo -n

clean:       _welcome
	@echo -e " $(ARROW) Cleaning\e[0m"
	@rm $(BUILD_DIR) $(VFS_DIR)/conv *.iso -rf
	@echo -n

directories: _welcome
	@echo -e " $(ARROW) Creating build directories\e[0m"
	@mkdir -p $(OBJDIRS) $(CFSDIRS) $(BUILD_DIR)/bin $(BUILD_DIR)/iso/fs/boot/grub $(BUILD_DIR)/iso/fs/fonts
	@echo -n

vfs:         _welcome _vfs $(VFS_FILES)
	@echo -e " $(ARROW) Inserting VFS into the code\e[0m"
	@mv 

Makefile:
	@echo -e " \e[1;31mStrange make bug prevented\e[0m"

# Compilation notification - do not remove
_compile:
	@echo -e " $(ARROW) Compiling\e[0m"
_vfs:
	@echo -e " $(ARROW) Converting binary files"

# Compilation routines
%.c:          _welcome directories _compile
	@echo -e " \e[0;32m Building C file:\e[0m \e[1;32m$@\e[0m"
	@$(CC) $@ -o $(patsubst $(SRC_DIR)/%.c,$(BUILD_DIR)/obj/%.c.o,$@) $(CFLAGS)
	@echo -n

%.s:          _welcome directories _compile
	@echo -e " \e[0;32m Building GAS file:\e[0m \e[1;32m$@\e[0m"
	@$(GAS) $@ -o $(patsubst $(SRC_DIR)/%.s,$(BUILD_DIR)/obj/%.s.o,$@)
	@echo -n

%.asm:        _welcome directories _compile
	@echo -e " \e[0;32m Building IAS file:\e[0m \e[1;32m$@\e[0m"
	@$(IASM) $@ -o $(patsubst $(SRC_DIR)/%.asm,$(BUILD_DIR)/obj/%.asm.o,$@) $(IASFLAGS)
	@echo -n
	
%:        _welcome directories _vfs
	@echo -e " \e[0;32m Converting binary file:\e[0m \e[1;32m$@\e[0m"
	mkdir -p $(VFS_SRCNAME)
	@mv file_* $(VFS_SRCNAME)/
	@echo "#include \"../$(VFS_SRCNAME)/$(VFS_VARNAME).h\"" >> include/vfs_tree.h
